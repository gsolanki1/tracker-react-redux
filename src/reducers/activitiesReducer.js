import uuid from "react-uuid";

const initialState = [
  // {
  //   id:1,
  //   name: "gym",
  //   duration: "1 hour",
  // },
  // {
  //   id:2,
  //   name: "TT",
  //   duration: "1/2 hour",
  // },
  // {
  //   id:3,
  //   name: "Swimming Pool",
  //   duration: "1 hour",
  // }
];

const activitiesReducer = (state= initialState, action) => {
  const { type, payload } = action;

  switch(type) {
    case "CREATE_ACTIVITY":
      return [...state, {
        id: uuid(),
        name: payload.name,
        duration: payload.duration
      }]
      case "DELETE_ACTIVITY":
        const copyState = [...state];
        //Find id object to remove
        const i = copyState.findIndex(x => x.id==payload.id)
        copyState.splice(i, 1)

        return [...copyState]
    default:
      return state;
  }

  return state;
};

export default activitiesReducer;