import { React } from "react";
import { useDispatch } from "react-redux";

const Activity = (props) => {
  const dispatch = useDispatch();
  const deleteActivity = (props) => {
    dispatch({
      type: "DELETE_ACTIVITY",
      payload: {
        id: props.id,
      }
    })
  }

  return (
    <div className={"activity-wrapper"}>
      <h2>Activity Data: </h2><br></br>
      <h3>Name- {props.name} Duration - {props.duration}</h3>
      <button onClick={deleteActivity}>Delete</button>
    </div>
  )
}

export default Activity;